{-# LANGUAGE RecordWildCards #-}
module IMDB.Platform.Marshaller where

import Orville.PostgreSQL
import IMDB.Platform.Model
import Data.Text (Text)
import Data.Time

movieMarshaller :: SqlMarshaller MovieWrite MovieRead
movieMarshaller =
    Movie
        <$> marshallReadOnly (marshallField (\Movie{..} -> movieID)
                movieIDField )
        <*> marshallField (\Movie{..} -> title) movieTitleField
        <*> marshallField (\Movie{..} -> status) (boundedTextField "status" 255)
        <*> marshallField (\Movie{..} -> releaseDate) (nullableField 
                (dateField "release_date"))
        <*> marshallReadOnly (marshallField (\Movie{..} -> createdAt) 
                createdAtField)
        <*> marshallReadOnly (marshallField (\Movie{..} -> updatedAt) 
                updatedAtField)

actorMarshaller :: SqlMarshaller ActorWrite ActorRead
actorMarshaller =
    Actor
        <$> marshallReadOnly (marshallField (\Actor{..} -> actorID) 
                actorIDField)
        <*> marshallField (\Actor{..} -> firstName) 
                (boundedTextField "first_name" 255)
        <*> marshallField (\Actor {..} -> lastName) 
                (nullableField (boundedTextField "last_name" 255))
        <*> marshallField (\Actor {..} -> gender) (boundedTextField "gender" 255)
        <*> marshallField (\Actor {..} -> age) 
                (nullableField (integerField "age"))

movieActorMarshaller :: SqlMarshaller MovieActor MovieActor
movieActorMarshaller =
    MovieActor
        <$> marshallField (\MovieActor{..} -> movieID) movieIDField
        <*>
            marshallField (\MovieActor{..} -> actorID) actorIDField

agentMarshaller :: SqlMarshaller AgentWrite AgentRead
agentMarshaller =
    Agent
        <$> marshallReadOnly (marshallField 
                (\Agent{..} -> agentID) agentIDField)
        <*> marshallField agentName agentNameField
        <*> marshallField salaryPerYear salaryField
        <*> marshallReadOnly (marshallField 
                (\Agent{..} -> createdAt) createdAtField)
        <*> marshallReadOnly (marshallField 
                (\Agent{..} -> updatedAt) updatedAtField)

actorAgentMarshaller :: SqlMarshaller ActorAgentWrite ActorAgentRead
actorAgentMarshaller = 
    ActorAgent
        <$> (marshallField 
                (\ActorAgent{..} -> actorID) actorIDField)
        <*> marshallField 
                (\ActorAgent{..} -> agentID) agentIDField
        <*> marshallReadOnly (marshallField 
                (\ActorAgent{..} -> createdAt) createdAtField)
        <*> marshallReadOnly (marshallField 
                (\ActorAgent{..} -> updatedAt) updatedAtField)

-- Movie Budget
movieBudgetMarshaller :: SqlMarshaller MovieBudgetWrite MovieBudgetRead
movieBudgetMarshaller =
    MovieBudget
        <$> marshallField (\MovieBudget{..} -> movieID) movieIDField
        <*> marshallField (\MovieBudget{..} -> budget) budgetField
        <*> marshallReadOnly (marshallField 
                (\MovieBudget{..} -> createdAt) createdAtField)
        <*> marshallReadOnly (marshallField 
                (\MovieBudget{..} -> updatedAt) updatedAtField)

-- Movie Box Office
movieBoxOfficeMarshaller :: SqlMarshaller MovieBoxOfficeWrite MovieBoxOfficeRead
movieBoxOfficeMarshaller =
    MovieBoxOffice
        <$> marshallField (\MovieBoxOffice{..} -> movieID) movieIDField
        <*> marshallField (\MovieBoxOffice{..} -> collection) collectionField
        <*> marshallReadOnly (marshallField 
                (\MovieBoxOffice{..} -> createdAt) createdAtField)
        <*> marshallReadOnly (marshallField 
                (\MovieBoxOffice{..} -> updatedAt) updatedAtField)

movieInfoMarshaller :: SqlMarshaller MovieInfo MovieInfo
movieInfoMarshaller = 
    MovieInfo
        <$> marshallReadOnly (marshallField 
                    (\MovieInfo{..} -> title) movieTitleField)
        <*> marshallReadOnly (marshallField 
                    (\MovieInfo{..} -> status) movieStatusField)
        <*> marshallReadOnly (marshallField 
                (\MovieInfo{..} -> releaseDate) (nullableField releaseDateField))
        <*> marshallReadOnly (marshallField (\MovieInfo{..} -> budget)
                (nullableField budgetField))
        <*> marshallReadOnly (marshallField (\MovieInfo{..} -> collection)
                (nullableField collectionField))

actorIDField :: FieldDefinition NotNull ActorID
actorIDField    = coerceField $ serialField "actor_id"

movieIDField :: FieldDefinition NotNull MovieID
movieIDField    = coerceField $ serialField "movie_id"

movieTitleField :: FieldDefinition NotNull Text
movieTitleField = boundedTextField "title" 255

agentIDField :: FieldDefinition NotNull AgentID
agentIDField    = coerceField $ serialField "agent_id"

agentNameField :: FieldDefinition NotNull Text
agentNameField  = boundedTextField "agent_name" 255

salaryField :: FieldDefinition NotNull Double
salaryField     = doubleField "salary_per_year"

createdAtField :: FieldDefinition NotNull UTCTime
createdAtField  = utcTimestampField "created_at"

updatedAtField :: FieldDefinition NotNull UTCTime
updatedAtField  = utcTimestampField "updated_at"

budgetField :: FieldDefinition NotNull Double
budgetField = doubleField "budget"

collectionField :: FieldDefinition NotNull Double
collectionField = doubleField "collection"

movieStatusField :: FieldDefinition NotNull Text
movieStatusField = boundedTextField "status" 255

releaseDateField :: FieldDefinition NotNull Day
releaseDateField = dateField "release_date"
