{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
module IMDB.Platform.Core where

-- Entry point of the application

import Control.Monad.IO.Unlift (MonadUnliftIO)
import Control.Monad.Reader
import Data.Typeable
import IMDB.Platform.API
import qualified IMDB.Platform.Graphql.Handler as G
import qualified IMDB.Platform.Handler as H
import IMDB.Platform.Types
import Network.Wai.Handler.Warp (run)
import qualified Orville.PostgreSQL as O
import Orville.PostgreSQL.Raw.Connection
import Servant
import System.Log.FastLogger
import Haxl.Core
import IMDB.Platform.DataSource
import Control.Concurrent.QSem
import IMDB.Platform.Common.Utils

server ::
  ( MonadUnliftIO m
  , Typeable m
  , Typeable w
  , Monoid w) =>
  ServerT MainAPI (AppT m w)
server =
  healthCheckHandler
    :<|> graphqlHandler
    :<|> addMovieHandler
    :<|> updateMovieHandler
    :<|> fetchAllMoviesHandler
    :<|> fetchMovieByTitleHandler
    :<|> fetchMoviesBetweenDatesHandler
    :<|> fetchActorHandler
    :<|> addActorHandler
    :<|> deleteActorHandler
    :<|> updateActorHandler
    :<|> addMovieActorHandler
    :<|> fetchMovieByIDHandler
    :<|> fetchActorsByMovieHandler
    :<|> deleteMovieActorHandler
    :<|> fetchMoviesByActorHandler
    :<|> addAgentHandler
    :<|> fetchAllAgentsHandler
    :<|> addActorAgentHandler
    :<|> viewActorsByAgentIDHandler
    :<|> fetchTitlesByActorIDHandler
    :<|> fetchMovieInfoHandler
  where
    fetchMovieInfoHandler          = H.fetchMovieInfoH
    fetchTitlesByActorIDHandler    = H.fetchTitlesByActorIDH
    viewActorsByAgentIDHandler     = H.viewActorsByAgentIDH
    addActorAgentHandler           = H.addActorAgentH
    addAgentHandler                = H.addAgentH
    fetchAllAgentsHandler          = H.fetchAllAgentsH
    fetchMoviesByActorHandler      = H.fetchMoviesByActorIDH
    deleteMovieActorHandler        = H.deleteMovieActorH
    fetchActorsByMovieHandler      = H.fetchActorsByMovieID
    fetchMovieByIDHandler          = H.fetchMovieByIDH
    addMovieActorHandler           = H.addMovieActorH
    graphqlHandler                 = G.graphqlH
    healthCheckHandler             = H.healthCheckH
    addMovieHandler                = H.addMovieH
    updateMovieHandler             = H.updateMovieH
    fetchAllMoviesHandler          = H.fetchAllMoviesH 
    fetchMovieByTitleHandler       = H.fetchMovieByTitle
    fetchMoviesBetweenDatesHandler = H.fetchMoviesBetweenDatesH
    fetchActorHandler              = H.fetchActorsH
    addActorHandler                = H.addActorH
    updateActorHandler             = H.updateActorH
    deleteActorHandler             = H.deleteActorH

convertApp :: 
  O.ConnectionPool ->
  LoggerSet ->
  Env () w   ->
  String ->
  AppT IO w a ->
  Handler a
convertApp pool loggerSet_ env0 logLevel appT =
  let orvilleState =
        O.newOrvilleState
          O.defaultErrorDetailLevel
          pool
      config = Config loggerSet_ (toLogLevel logLevel)
      myAppState = MyAppState config orvilleState env0
   in Handler $ runReaderT (getApp appT) myAppState

allServer :: (Typeable w,Monoid w)
          => ConnectionPool 
          -> LoggerSet 
          -> Env () w
          -> String
          -> Server MainAPI
allServer pool loggerSet_ env0 logLevel =
  hoistServer (Proxy :: Proxy MainAPI) (convertApp pool loggerSet_ env0 logLevel) server

app :: (Typeable w,Monoid w) 
    => ConnectionPool 
    -> LoggerSet 
    -> Env () w 
    -> String
    -> Application
app pool loggerSet_ env0 logLevel = 
    serve (Proxy :: Proxy MainAPI) (allServer pool loggerSet_ env0 logLevel)

connectionOptions :: String -> ConnectionOptions
connectionOptions connString =
  ConnectionOptions
    { connectionString = connString,
        -- "dbname=sample_orville host=localhost user=tushar password=1234",
      connectionNoticeReporting = DisableNoticeReporting,
      connectionPoolStripes = OneStripePerCapability,
      connectionPoolMaxConnections = MaxConnectionsPerStripe 1,
      connectionPoolLingerTime = 10
    }

startApp :: CMDArgs -> IO ()
startApp CMDArgs{..} = do
  pool <- createConnectionPool $ connectionOptions connString
  loggerSet_ <- newFileLoggerSet defaultBufSize logFilePath -- "/home/user/haskell/imdb-clone/imdb-logs.txt"
  -- [Int] is used only because otherwise compiler give error saying w does not have monoid instance
  sem <- newQSem 10 -- 10 threads
  (env0 :: Env () [Int]) <- initEnv (stateSet (IMDBState pool sem) stateEmpty) ()
  putStrLn "Application is running at port 8082"
  run 8082 $ app pool loggerSet_ env0 logLevel
