{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DuplicateRecordFields #-}
module IMDB.Platform.Graphql.Core where

import           Data.Morpheus.Document (importGQLDocument)
import           Data.Text (Text)
import           Data.Morpheus.Types
import           IMDB.Platform.Model
import           IMDB.Platform.Query
import           Data.Coerce
import           Data.Time
import           Data.Time.Calendar.OrdinalDate
import           IMDB.Platform.Types
import           Control.Monad.IO.Unlift (MonadUnliftIO)
import           GHC.Int (Int32)
import           Data.Maybe

idToInt :: forall a. (Coercible a Int32) => a -> Int32
idToInt = coerce

releaseDateToYearMaybe :: Maybe Day -> Maybe Int
releaseDateToYearMaybe (Just d) = Just $ (fromIntegral.fst.toOrdinalDate) d
releaseDateToYearMaybe Nothing = Nothing

importGQLDocument "/home/user/haskell/imdb-clone/src/IMDB/Platform/schema.gql"

rootResolver :: MonadUnliftIO m => RootResolver (AppT m w) () Query Mutation Undefined
rootResolver = RootResolver { 
    queryResolver = Query {
            movies = resolveMovies
          , actors = resolveActors
          , movie = resolveMovie
          , actor = resolveActor
        }
  , mutationResolver = Mutation {
            createMovie = resolveCreateMovie
          , createActor = resolveCreateActor
          , addActorToMovie = resolveActorToMovie
          , addMovieToActor = resolveMovieToActor
        }
    , subscriptionResolver = undefined
    }


-- Query

resolveMovie :: MonadUnliftIO m =>
        (Arg "mId" Int) -> 
        Resolver QUERY () (AppT m w) (Maybe (MovieGQL (Resolver QUERY () (AppT m w))))
resolveMovie mId = do
    res <- lift $ fetchMovieByIDQ (MovieID $ fromIntegral (argValue mId))
    case res of
        Nothing -> pure Nothing
        Just val -> pure $ Just $ toMovieGQL val

resolveActor :: MonadUnliftIO m =>
        (Arg "aId" Int) ->
        Resolver QUERY () (AppT m w) (Maybe (ActorGQL (Resolver QUERY () (AppT m w))))
resolveActor aId = do
    res <- lift $ fetchActorByIDQ (ActorID $ fromIntegral (argValue aId))
    case res of
        Nothing -> pure Nothing
        Just val -> pure $ Just $ toActorGQL val
 
resolveMovies
        :: MonadUnliftIO m => 
          Resolver
             QUERY () (AppT m w) [MovieGQL (Resolver QUERY () (AppT m w))]
resolveMovies = do
    res <- lift fetchAllMoviesQ
    pure $ map toMovieGQL res

resolveActors :: MonadUnliftIO m =>
              Resolver
                QUERY () (AppT m w) [ActorGQL (Resolver QUERY () (AppT m w))]
resolveActors = do
    res <- lift fetchAllActorsQ
    pure $ map toActorGQL res


-- Mutation

resolveCreateMovie :: MonadUnliftIO m
                   => CreateMovieArgs 
                   -> Resolver MUTATION () (AppT m w) 
                        (MovieGQL (Resolver MUTATION () (AppT m w)))
resolveCreateMovie CreateMovieArgs{..} = do
    res <- lift $ insertMovieQ $ Movie {
        movieID = ()
      , title = title
      , releaseDate = Just $ fromOrdinalDate (fromIntegral releaseDate) 0
      , status = "Filming"
      , createdAt = ()
      , updatedAt = () -- default values
    }
    pure $ toMovieGQL res


resolveCreateActor :: MonadUnliftIO m
                   => CreateActorArgs
                   -> Resolver MUTATION () (AppT m w)
                        (ActorGQL (Resolver MUTATION () (AppT m w)))
resolveCreateActor CreateActorArgs{..} = do
    res <- lift $ addActorQ $ Actor {
        firstName = name
      , age = fromIntegral <$> age
      ,         actorID = ActorID 1
      , lastName = Nothing
      , gender = "Male"
    }
    pure $ toActorGQL res


resolveActorToMovie :: MonadUnliftIO m 
                    => AddActorToMovieArgs 
                    -> Resolver MUTATION () (AppT m w)
                        Bool
resolveActorToMovie AddActorToMovieArgs{..} = do
    lift $ addMovieActorQ (MovieID $ fromIntegral movieId) 
                    (ActorID $ fromIntegral actorId)
    pure True

resolveMovieToActor :: MonadUnliftIO m
                    => AddMovieToActorArgs
                    -> Resolver MUTATION () (AppT m w)
                        Bool
resolveMovieToActor AddMovieToActorArgs{..} = do
    lift $ addMovieActorQ (MovieID $ fromIntegral movieId) 
                    (ActorID $ fromIntegral actorId)
    pure True

-- Helpers
toMovieGQL :: (Coercible a Int32,Monad m) => Movie a b -> MovieGQL m
toMovieGQL Movie{..} = MovieGQL {
    id = return $ fromIntegral $ idToInt movieID
  , title = return title
  , year = return $ releaseDateToYearMaybe releaseDate
}

toActorGQL :: (Coercible a Int32,Monad m) => Actor a -> ActorGQL m
toActorGQL  Actor{..} = ActorGQL {
            id = return $ fromIntegral $ idToInt actorID
          , name = return $ firstName <> " " <> fromMaybe "" lastName
          , age = return $ fromIntegral <$> age
        }

