module IMDB.Platform.Graphql.Handler where

import           Data.Morpheus.Types.IO (GQLRequest,GQLResponse)
import           Data.Morpheus (interpreter)
import           IMDB.Platform.Graphql.Core
import           IMDB.Platform.Types
import           Control.Monad.IO.Unlift (MonadUnliftIO)
import           Data.Typeable

api :: (MonadUnliftIO m,Typeable m,Typeable w) => GQLRequest -> AppT m w GQLResponse
api = interpreter rootResolver

graphqlH :: (MonadUnliftIO m,Typeable m,Typeable w) => GQLRequest -> AppT m w GQLResponse
graphqlH body = api body
