{-# LANGUAGE RecordWildCards #-}
module IMDB.Platform.Common.Utils where

import IMDB.Platform.Model
import IMDB.Platform.Types

getActorID :: MovieActor -> ActorID
getActorID MovieActor {..} = actorID

getMovieID :: MovieActor -> MovieID
getMovieID MovieActor {..} = movieID

getActorAgentReadID :: ActorAgentRead -> ActorID
getActorAgentReadID ActorAgent {..} = actorID

toLogLevel :: String -> LogLevel 
toLogLevel "Debug" = LevelDebug
toLogLevel "Info" = LevelInfo
toLogLevel "Warn" = LevelWarn
toLogLevel "Error" = LevelError
toLogLevel _ = LevelDebug
