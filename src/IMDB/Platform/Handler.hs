{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module IMDB.Platform.Handler where

import Data.Text (Text)
import IMDB.Platform.Types
import IMDB.Platform.Model
import IMDB.Platform.Query
import Control.Monad.IO.Class
import Control.Monad.IO.Unlift (MonadUnliftIO)
import IMDB.Platform.Log
import IMDB.Platform.DataSource
import Haxl.Core
import Control.Monad.Reader

healthCheckH :: MonadIO m => AppT m w Text
healthCheckH = return "System is fine!"

-- Movie Handlers
addMovieH :: MonadUnliftIO m => MovieWrite -> AppT m w MovieRead
addMovieH = insertMovieQ

updateMovieH :: MonadUnliftIO m => MovieWrite -> MovieID -> AppT m w (Maybe MovieRead)
updateMovieH = updateMovieQ

fetchAllMoviesH :: (MonadIO m,Monoid w) => AppT m w [MovieRead]
fetchAllMoviesH = do
    env0 <- asks haxlEnv
    liftIO $ runHaxl env0 (dataFetch GetMovies)

fetchMovieByTitle :: (Monoid w,MonadIO m) => Text -> AppT m w (Maybe MovieRead)
fetchMovieByTitle title0 = asks haxlEnv >>= \env0 -> 
    liftIO $ runHaxl env0 (dataFetch (GetMovieByTitle title0))

fetchMoviesBetweenDatesH :: (MonadIO m,Monoid w)
                         => FetchMoviesBetweenDatesData
                         -> AppT m w [MovieRead]
fetchMoviesBetweenDatesH f = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetMoviesBetweenDates f))

fetchMovieByIDH :: (MonadIO m,Monoid w) => MovieID -> AppT m w (Maybe MovieRead)
fetchMovieByIDH movieID0 = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetMovieByID movieID0))

-- Actor API
fetchActorsH :: (MonadIO m,Monoid w) => AppT m w [ActorRead]
fetchActorsH = do
    logDebug "whatever"
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch GetActors)

updateActorH:: MonadUnliftIO m => ActorWrite -> ActorID -> AppT m w ()
updateActorH = updateActorQ

addActorH :: MonadUnliftIO m => ActorWrite -> AppT m w ActorRead
addActorH = addActorQ

deleteActorH :: MonadUnliftIO m => ActorID -> AppT m w ()
deleteActorH = deleteActorQ

-- Movie Actor API
addMovieActorH :: MonadUnliftIO m => MovieID -> ActorID -> AppT m w ()
addMovieActorH = addMovieActorQ

deleteMovieActorH :: MonadUnliftIO m => MovieID -> ActorID -> AppT m w ()
deleteMovieActorH mID aID = deleteMovieActorQ $ MovieActor mID aID

-- Fetch Actors who acted in a particular movie
fetchActorsByMovieID :: (MonadIO m,Monoid w) => MovieID -> AppT m w [ActorRead]
fetchActorsByMovieID movieID0 = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetActorsByMovieID movieID0))

-- Fetch Movies where particular
fetchMoviesByActorIDH :: (MonadIO m,Monoid w) => ActorID -> AppT m w [MovieRead]
fetchMoviesByActorIDH actorID0 = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetMoviesByActorID actorID0))

-- Agents API
addAgentH :: MonadUnliftIO m => AgentWrite -> AppT m w ()
addAgentH = addAgentQ

fetchAllAgentsH :: (MonadIO m,Monoid w) => AppT m w [AgentRead]
fetchAllAgentsH = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch GetAgents)

addActorAgentH :: MonadUnliftIO m => ActorAgentWrite -> AppT m w ()
addActorAgentH = addActorAgentQ

viewActorsByAgentIDH :: (MonadIO m,Monoid w) => AgentID -> AppT m w [ActorRead]
viewActorsByAgentIDH agentID0 = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetActorsByAgentID agentID0))

fetchTitlesByActorIDH :: (MonadIO m,Monoid w) => ActorID -> AppT m w [Text]
fetchTitlesByActorIDH aID = 
    asks haxlEnv >>= \env0 -> 
        liftIO $ runHaxl env0 (dataFetch (GetTitlesByActorID aID))

fetchMovieInfoH :: (MonadIO m,Monoid w) 
                => AppT m w [MovieInfo]
fetchMovieInfoH =
    asks haxlEnv >>= \env0 ->
        liftIO $ runHaxl env0 $ dataFetch GetMovieInfo

