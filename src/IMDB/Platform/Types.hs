{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric,DeriveAnyClass #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module IMDB.Platform.Types where

import GHC.Generics
import Data.Aeson
import Data.Time
import qualified Control.Monad.IO.Class as MIO
import Control.Monad.Except
import Servant
import Control.Monad.Reader
import qualified Orville.PostgreSQL as O
import qualified Orville.PostgreSQL.UnliftIO as OrvilleUnliftIO
import UnliftIO
import Control.Monad ((<=<))
import System.Log.FastLogger
import Haxl.Core (Env(..))
import Data.Hashable (Hashable)
import Dhall

type ServerExcept m = ExceptT ServerError m

data CMDArgs = CMDArgs {
    connString  :: String
  , logFilePath :: String
  , logLevel    :: String
}

data Config = Config {
    loggerSet :: LoggerSet
  , minLogLevel :: MinLogLevel
} 

data MyAppState w = MyAppState{ 
    appConfig       :: Config
  , appOrvilleState :: O.OrvilleState
  , haxlEnv         :: Env () w
}

newtype AppT m w a = AppT { 
  getApp ::ReaderT (MyAppState w) (ServerExcept m) a
} deriving newtype (
        Functor
      , Applicative
      , Monad
      , MIO.MonadIO
      , MonadReader (MyAppState w)
      )

instance Monad m => O.HasOrvilleState (AppT m w) where
  askOrvilleState = AppT (asks appOrvilleState)
  localOrvilleState f (AppT reader_) = 
        AppT $ local (\myAppState -> myAppState {
            appOrvilleState = f (appOrvilleState myAppState)
        }) reader_

instance (MonadIO m,UnliftIO.MonadUnliftIO m) => UnliftIO.MonadUnliftIO (AppT m w) where
  withRunInIO inner = AppT $ ReaderT $ \r ->
      withRunInIO $ \runInIO ->
        inner (runInIO . runAppT r)

runAppT :: MyAppState w -> AppT m w a -> ServerExcept m a
runAppT r (AppT m) = runReaderT m r

-- taken from https://github.com/freckle/yesod-auth-oauth2/blob/acb69f8da40b9c91b4020296ce105119e76fdf1d/src/UnliftIO/Except.hs#L9
instance (MonadUnliftIO m, Exception e) => MonadUnliftIO (ExceptT e m) where
  withRunInIO exceptToIO = ExceptT $ try $ do
    withRunInIO $ \runInIO ->
      exceptToIO (runInIO . (either throwIO pure <=< runExceptT))

-- https://hackage.haskell.org/package/orville-postgresql-1.0.0.0/docs/Orville-PostgreSQL-UnliftIO.html#v:liftWithConnectionViaUnliftIO
instance (MonadIO m,UnliftIO.MonadUnliftIO m) => O.MonadOrvilleControl (AppT m w) where
  liftWithConnection = OrvilleUnliftIO.liftWithConnectionViaUnliftIO
  liftCatch = OrvilleUnliftIO.liftCatchViaUnliftIO
  liftMask = OrvilleUnliftIO.liftMaskViaUnliftIO

instance (Monad m,MonadIO m,UnliftIO.MonadUnliftIO m) => O.MonadOrville (AppT m w) where

-- Log types

data LogLevel = LevelDebug
              | LevelInfo
              | LevelWarn
              | LevelError
  deriving (Eq,Ord)

instance Show LogLevel where
  show LevelDebug = "[Debug]"
  show LevelInfo = "[Info]"
  show LevelWarn = "[Warn]"
  show LevelError = "[Error]"

type MinLogLevel = LogLevel
-- End of log types

data MovieStatus = PreProduction
                 | PostProduction
                 | Filming
                 | Released
    deriving (Eq,Show,FromJSON,ToJSON,Generic)

data Gender = Male | Female
    deriving (Eq,Show,FromJSON,ToJSON,Generic)

data FetchMoviesBetweenDatesData = FetchMoviesBetweenDatesData {
    startDate :: Day
  , endDate :: Day
} deriving (Show,Eq,Generic,ToJSON,FromJSON,Hashable)
