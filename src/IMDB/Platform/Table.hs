{-# LANGUAGE RecordWildCards #-}
module IMDB.Platform.Table where

import IMDB.Platform.Marshaller
import Orville.PostgreSQL
import IMDB.Platform.Model

-- Movies
movieTable :: TableDefinition (HasKey MovieID) MovieWrite MovieRead
movieTable =
    mkTableDefinition
        "movies"
            (primaryKey ((coerceField.serialField) "movie_id"))
                movieMarshaller
-- Actor
actorTable :: TableDefinition (HasKey ActorID) ActorWrite ActorRead
actorTable =
    mkTableDefinition
        "actor"
            (primaryKey (coerceField (serialField "actor_id")))
                actorMarshaller

-- Movie Actor
movieActorTable :: TableDefinition (HasKey MovieActor) MovieActor MovieActor
movieActorTable =
    mkTableDefinition
        "movie_actor"
            movieActorCompositeKey
                movieActorMarshaller

movieActorCompositeKey :: PrimaryKey MovieActor
movieActorCompositeKey = 
    compositePrimaryKey 
        (primaryKeyPart (\MovieActor{..} -> movieID) movieIDField)
        [primaryKeyPart (\MovieActor{..} -> actorID) actorIDField]

-- Agent
agentTable :: TableDefinition (HasKey AgentID) AgentWrite AgentRead
agentTable =
    mkTableDefinition
        "agent"
            (primaryKey agentIDField)
                agentMarshaller

-- ActorAgent
actorAgentTable :: TableDefinition (HasKey ActorID) ActorAgentWrite ActorAgentRead
actorAgentTable =
    mkTableDefinition
        "actor_agent"
            (primaryKey actorIDField)
                actorAgentMarshaller

-- MovieBudget
movieBudgetTable :: TableDefinition (HasKey MovieID) MovieBudgetWrite MovieBudgetRead
movieBudgetTable =
    mkTableDefinition
        "movie_budget"
            (primaryKey movieIDField)
                movieBudgetMarshaller

--MovieBoxOffice
movieBoxOfficeTable :: TableDefinition (HasKey MovieID) MovieBoxOfficeWrite MovieBoxOfficeRead
movieBoxOfficeTable =
    mkTableDefinition
        "box_office_collection"
            (primaryKey movieIDField)
                movieBoxOfficeMarshaller
