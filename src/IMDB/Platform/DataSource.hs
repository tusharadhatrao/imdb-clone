{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
module IMDB.Platform.DataSource where

import Data.Hashable
import Haxl.Core
import IMDB.Platform.Model
import IMDB.Platform.Query
import Orville.PostgreSQL (ConnectionPool,runOrville)
import Control.Concurrent.QSem
import Control.Concurrent.Async
import Control.Exception as Exp
import Data.Text (Text)
import IMDB.Platform.Types
import Control.Monad (void)

data IMDBReq a where
  GetMovies :: IMDBReq [MovieRead]
  GetMovieByTitle :: Text -> IMDBReq (Maybe MovieRead)
  GetMovieByID :: MovieID -> IMDBReq (Maybe MovieRead)
  GetMoviesBetweenDates :: FetchMoviesBetweenDatesData -> IMDBReq [MovieRead]
  GetActors :: IMDBReq [ActorRead]
  GetMoviesByActorID :: ActorID -> IMDBReq [MovieRead]
  GetAgents :: IMDBReq [AgentRead]
  GetActorsByAgentID :: AgentID -> IMDBReq [ActorRead]
  GetActorsByMovieID :: MovieID -> IMDBReq [ActorRead]
  GetTitlesByActorID :: ActorID -> IMDBReq [Text]
  GetMovieInfo       :: IMDBReq [MovieInfo]

deriving instance Eq (IMDBReq a)
deriving instance Show (IMDBReq a)

instance ShowP IMDBReq where showp = show

instance Hashable (IMDBReq a) where
    hashWithSalt s GetMovies                     = hashWithSalt s (0 :: Int)
    hashWithSalt s GetMovieInfo                  = hashWithSalt s (0 :: Int)
    hashWithSalt s (GetMovieByTitle title0)      = hashWithSalt s (1 :: Int, title0)
    hashWithSalt s (GetMovieByID movieID0)       = hashWithSalt s (2 :: Int, movieID0)
    hashWithSalt s GetActors                     = hashWithSalt s (0 :: Int)
    hashWithSalt s (GetMoviesByActorID actorID0) = hashWithSalt s (1 :: Int, actorID0)
    hashWithSalt s GetAgents                     = hashWithSalt s (0 :: Int) 
    hashWithSalt s (GetActorsByAgentID agentID0) = hashWithSalt s (1 :: Int, agentID0)
    hashWithSalt s (GetActorsByMovieID movieID0) = hashWithSalt s (2 :: Int, movieID0)
    hashWithSalt s (GetTitlesByActorID actorID0) = hashWithSalt s (2 :: Int, actorID0)
    hashWithSalt s (GetMoviesBetweenDates betweenDates) = 
                        hashWithSalt s (2 :: Int,betweenDates)

instance StateKey IMDBReq where
  data State IMDBReq = IMDBState ConnectionPool QSem

instance DataSourceName IMDBReq where
  dataSourceName _ = "IMDB"

instance DataSource u IMDBReq where
  fetch = imdbFetch

imdbFetch :: State IMDBReq -> Flags -> u -> PerformFetch IMDBReq
imdbFetch (IMDBState pool threads) _ _ = BackgroundFetch $ mapM_ (asyncFunc threads pool)

asyncFunc :: QSem -> ConnectionPool -> BlockedFetch IMDBReq -> IO ()
asyncFunc sem pool (BlockedFetch req resVar) =
    void $ async $ bracket_ (waitQSem sem) (signalQSem sem) $ do
      (e :: Either SomeException a) <- Exp.try $ runOrville pool =<< 
        case req of
          GetMovies                  ->  pure   fetchAllMoviesQ
          GetActors                  ->  pure   fetchAllActorsQ
          GetAgents                  ->  pure   fetchAllAgentsQ
          GetMovieInfo               -> pure $ fetchMovieBudgetAndTitleQ
          GetMovieByTitle title0     ->  pure $ fetchMovieByTitleQ title0
          GetMovieByID movieID0      ->  pure $ fetchMovieByIDQ movieID0
          GetMoviesByActorID actorID ->  pure $ fetchMoviesByActorIDQ actorID
          GetActorsByAgentID agentID ->  pure $ viewActorsByAgentIDQ agentID
          GetActorsByMovieID movieID ->  pure $ fetchActorsByMovieIDQ movieID
          GetTitlesByActorID actorID ->  pure $ fetchTitlesByActorIDQ actorID 
          GetMoviesBetweenDates FetchMoviesBetweenDatesData{..} ->
                    pure $ fetchMoviesBetweenReleaseDatesQ startDate endDate

      case e of
        Left errMsg -> putFailure resVar errMsg
        Right res -> putSuccess resVar res
