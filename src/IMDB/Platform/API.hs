{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module IMDB.Platform.API where

import Data.Morpheus.Types.IO (GQLRequest, GQLResponse)
import Data.Text (Text)
import IMDB.Platform.Model
import IMDB.Platform.Types
import Servant

type MainAPI =
  CheckHealthAPI
    :<|> GraphqlAPI
    :<|> AddMovieAPI
    :<|> UpdateMovieAPI
    :<|> FetchAllMoviesAPI
    :<|> FetchMovieByTitleAPI
    :<|> FetchMoviesBetweenDatesAPI
    :<|> ViewActorsAPI
    :<|> AddActorAPI
    :<|> DeleteActorAPI
    :<|> UpdateActorAPI
    :<|> AddMovieActorAPI
    :<|> FetchMovieByIDAPI
    :<|> FetchActorsByMovieAPI
    :<|> DeleteMovieActorAPI
    :<|> FetchMoviesByActorAPI
    :<|> AddAgentAPI
    :<|> ViewAgentsAPI
    :<|> AddActorAgentAPI
    :<|> ViewActorsByAgentIDAPI
    :<|> FetchTitlesByActorIDAPI
    :<|> FetchMovieInfo

type CheckHealthAPI =
  "check-health"
    :> Get '[JSON] Text

-- Movie APIs
type AddMovieAPI =
  "add_movie"
    :> ReqBody '[JSON] MovieWrite
    :> Post '[JSON] MovieRead

type UpdateMovieAPI =
  "update_movie"
    :> ReqBody '[JSON] MovieWrite
    :> Capture "movieID" MovieID
    :> Put '[JSON] (Maybe MovieRead)

type FetchAllMoviesAPI =
  "fetch_movies"
    :> Get '[JSON] [MovieRead]

type FetchMovieByTitleAPI =
  "fetch_movie_title"
    :> Capture "title" Text
    :> Get '[JSON] (Maybe MovieRead)

type FetchMovieByIDAPI =
  "fetch_movie_info"
    :> Capture "movieID" MovieID
    :> Get '[JSON] (Maybe MovieRead)

type FetchMoviesBetweenDatesAPI =
  "fetch_movies_between_dates"
    :> ReqBody '[JSON] FetchMoviesBetweenDatesData
    :> Get '[JSON] [MovieRead]

-- Actor APIs
type ViewActorsAPI =
  "view_actors"
    :> Get '[JSON] [ActorRead]

type AddActorAPI =
  "add_actor"
    :> ReqBody '[JSON] ActorWrite
    :> Post '[JSON] ActorRead

type DeleteActorAPI =
  "delete_actor"
    :> Capture "actorID" ActorID
    :> Delete '[JSON] ()

type UpdateActorAPI =
  "update_actor"
    :> ReqBody '[JSON] ActorWrite
    :> Capture "actorID" ActorID
    :> Put '[JSON] ()

type GraphqlAPI =
  "graphql"
    :> ReqBody '[JSON] GQLRequest
    :> Post '[JSON] GQLResponse

-- MovieActor APIs

type AddMovieActorAPI =
  "add_movie_actor"
    :> Capture "movieID" MovieID
    :> Capture "actorID" ActorID
    :> Post '[JSON] ()

type DeleteMovieActorAPI =
  "delete_movie_actor"
    :> Capture "movieID" MovieID
    :> Capture "actorID" ActorID
    :> Delete '[JSON] ()

type FetchActorsByMovieAPI =
  "fetch_actor_by_movie"
    :> Capture "movieID" MovieID
    :> Get '[JSON] [ActorRead]

type FetchMoviesByActorAPI =
  "fetch_movie_by_actor"
    :> Capture "actorID" ActorID
    :> Get '[JSON] [MovieRead]

-- Agent APIs

type AddAgentAPI =
  "add_agent"
    :> ReqBody '[JSON] AgentWrite
    :> Post '[JSON] ()

type ViewAgentsAPI =
  "view_agents"
    :> Get '[JSON] [AgentRead]

-- Actor Agent APIs

type AddActorAgentAPI =
  "add_actor_agent"
    :> ReqBody '[JSON] ActorAgentWrite
    :> Post '[JSON] ()

type ViewActorsByAgentIDAPI =
  "view_actors_by_agent"
    :> Capture "agentID" AgentID
    :> Get '[JSON] [ActorRead]

type FetchTitlesByActorIDAPI =
  "fetch_titles_by_actorID"
    :> Capture "actorID" ActorID
    :> Get '[JSON] [Text]

type FetchMovieInfo = 
    "fetch_movie_info"
    :> Get '[JSON] [MovieInfo]
