{-# LANGUAGE DeriveAnyClass,DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}
module IMDB.Platform.Model where

import Data.Text (Text)
import Data.Time
import GHC.Generics
import Data.Aeson
import GHC.Int (Int32)
import Web.HttpApiData
import Data.Hashable
import Dhall

-- Movie table
newtype MovieID = MovieID Int32 
    deriving newtype (Eq,Show,FromJSON,ToJSON,Ord,FromHttpApiData,Hashable)

data Movie a b = Movie {
    movieID :: a
  , title   :: Text
  , status  :: Text
  , releaseDate :: Maybe Day
  , createdAt :: b
  , updatedAt :: b
} deriving (Eq,Show,FromJSON,ToJSON,Generic)

type MovieRead = Movie MovieID UTCTime
type MovieWrite = Movie () ()

-- Actor Table
newtype ActorID = ActorID Int32
    deriving newtype (Show,Eq,ToJSON,FromJSON,Ord,FromHttpApiData,Hashable,FromDhall)

data Actor a = Actor {
    actorID   :: a
  , firstName :: Text
  , lastName  :: Maybe Text
  , gender    :: Text
  , age       :: Maybe Int32
} deriving (Show,Eq,Generic,ToJSON,FromJSON,FromDhall)

type ActorRead = Actor ActorID
type ActorWrite = Actor ActorID

-- MovieActor Table (Many to Many)
data MovieActor = MovieActor {
    movieID :: MovieID    
  , actorID :: ActorID
} deriving (Show,Eq,Generic,ToJSON,FromJSON)

-- Agent Table
newtype AgentID = AgentID Int32
    deriving newtype (Eq,Show,ToJSON,FromJSON,Ord,FromHttpApiData,Hashable)

data Agent a b = Agent {
    agentID         :: a
  , agentName       :: Text
  , salaryPerYear   :: Double
  , createdAt       :: b 
  , updatedAt       :: b
} deriving (Show,Eq,Generic,ToJSON,FromJSON)

type AgentRead = Agent AgentID UTCTime
type AgentWrite = Agent () ()

-- ActorAgent Table (One to Many table. Agent can have multiple actors as client
-- while Actor can have at max 1 client at a time)

data ActorAgent time = ActorAgent {
    actorID :: ActorID
  , agentID :: AgentID
  , createdAt :: time
  , updatedAt :: time
} deriving (Show,Eq,Generic,ToJSON,FromJSON)

type ActorAgentRead = ActorAgent UTCTime
type ActorAgentWrite = ActorAgent ()

-- Movie_budget table

data MovieBudget a = MovieBudget {
    movieID :: MovieID
  , budget  :: Double
  , createdAt :: a
  , updatedAt :: a
} deriving (Show,Eq,Generic,ToJSON,FromJSON)

type MovieBudgetRead = MovieBudget UTCTime
type MovieBudgetWrite = MovieBudget ()

-- Box_office_collection

data MovieBoxOffice a = MovieBoxOffice {
    movieID :: MovieID
  , collection :: Double
  , createdAt :: a
  , updatedAt :: a
} deriving (Show,Eq,Generic,ToJSON,FromJSON)

type MovieBoxOfficeRead = MovieBoxOffice UTCTime
type MovieBoxOfficeWrite = MovieBoxOffice ()

-- Custom types for fetching data


data MovieInfo = MovieInfo {
    title :: Text
  , status :: Text
  , releaseDate :: Maybe Day
  , budget   :: Maybe Double
  , collection :: Maybe Double
} deriving (Show,Eq,Generic,ToJSON,FromJSON)
