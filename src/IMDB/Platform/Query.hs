{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module IMDB.Platform.Query where

import Data.Text (Text)
import Data.Time
import Data.Coerce

import IMDB.Platform.Marshaller
import IMDB.Platform.Model
import IMDB.Platform.Table
import IMDB.Platform.Common.Utils

import           Orville.PostgreSQL (MonadOrville)
import qualified Orville.PostgreSQL as O
import qualified Orville.PostgreSQL.Raw.SqlValue as SqlValue
import qualified Orville.PostgreSQL.Expr as Expr
import           Orville.PostgreSQL.Expr
import qualified Orville.PostgreSQL.Execution as Execution
import           Orville.PostgreSQL.Plan 
  ( chain,
    execute,
    findAll,
    findMaybeOne,
    findOne,
    focusParam,
    planList,
  )


nullOr :: (a -> SqlValue.SqlValue) -> Maybe a -> SqlValue.SqlValue
nullOr = maybe SqlValue.sqlNull

mkColumnReference :: String -> String -> ValueExpression
mkColumnReference tName colName =
    columnReference $
        aliasQualifyColumn 
            (Just $ stringToAliasExpr tName) (columnName colName)

-- Movie Queries
insertMovieQ :: (O.MonadOrville m) => MovieWrite -> m MovieRead
insertMovieQ = O.insertAndReturnEntity movieTable

updateMovieQ ::
  (MonadOrville m) =>
  MovieWrite ->
  MovieID ->
  m (Maybe MovieRead)
updateMovieQ movie movieID = O.updateAndReturnEntity movieTable movieID movie

fetchAllMoviesQ :: (O.MonadOrville m) => m [MovieRead]
fetchAllMoviesQ = O.findEntitiesBy movieTable O.emptySelectOptions

fetchMovieByTitleQ ::
  (MonadOrville m) =>
  Text ->
  m (Maybe MovieRead)
fetchMovieByTitleQ = execute (findMaybeOne movieTable movieTitleField)

-- Fetch movies release between given two release days order by release date
fetchMoviesBetweenReleaseDatesQ ::
  (MonadOrville m) =>
  Day ->
  Day ->
  m [MovieRead]
fetchMoviesBetweenReleaseDatesQ startDate endDate =
  O.findEntitiesBy
    movieTable
    (betweenDates <> orderByReleaseDates)
  where
    betweenDates =
      O.where_
        ( O.fieldGreaterThanOrEqualTo releaseDateField startDate
            Expr..&& O.fieldLessThanOrEqualTo releaseDateField endDate
        )
    orderByReleaseDates =
      O.orderBy
        ( O.orderByColumnName
            (O.fieldColumnName Nothing releaseDateField)
            Expr.descendingOrder
        )

-- Actor queries
fetchAllActorsQ :: (MonadOrville m) => m [ActorRead]
fetchAllActorsQ = O.findEntitiesBy actorTable O.emptySelectOptions

updateActorQ :: (MonadOrville m) => ActorWrite -> ActorID -> m ()
updateActorQ actor aID = O.updateEntity actorTable aID actor

addActorQ :: (MonadOrville m) => ActorWrite -> m ActorRead
addActorQ = O.insertAndReturnEntity actorTable

deleteActorQ :: (MonadOrville m) => ActorID -> m ()
deleteActorQ = O.deleteEntity actorTable

fetchActorByIDQ :: (MonadOrville m) => ActorID -> m (Maybe ActorRead)
fetchActorByIDQ = execute (findMaybeOne actorTable actorIDField)

-- Movie Actor Queries

addMovieActorQ :: (MonadOrville m) => MovieID -> ActorID -> m ()
addMovieActorQ m a = O.insertEntity movieActorTable (MovieActor m a)

deleteMovieActorQ :: (MonadOrville m) => MovieActor -> m ()
deleteMovieActorQ = O.deleteEntity movieActorTable

fetchMovieByIDQ :: (MonadOrville m) => MovieID -> m (Maybe MovieRead)
fetchMovieByIDQ = execute (findMaybeOne movieTable movieIDField)

fetchActorsByMovieIDQ :: (MonadOrville m) => MovieID -> m [ActorRead]
fetchActorsByMovieIDQ = execute
    ( findAll movieActorTable movieIDField
        `chain` planList (focusParam getActorID $ findOne actorTable actorIDField)
    )

fetchMoviesByActorIDQ :: (MonadOrville m) => ActorID -> m [MovieRead]
fetchMoviesByActorIDQ = execute
    ( findAll movieActorTable actorIDField
        `chain` planList (focusParam getMovieID $ findOne movieTable movieIDField)
    )

fetchTitlesByActorIDQ :: (MonadOrville m) => ActorID -> m [Text]
fetchTitlesByActorIDQ actorID = do
   Execution.executeAndDecode 
            Execution.SelectQuery 
                (fetchAllMoviesQExpr actorID)
                    (O.annotateSqlMarshallerEmptyAnnotation 
                        (O.marshallField (\Movie{..} -> title) movieTitleField))

{-
SQL Query:
select title 
from movies m 
	inner join movie_actor ma on m.movie_id = ma.movie_id 
where actor_id = 1;
-}
fetchAllMoviesQExpr :: ActorID -> QueryExpr
fetchAllMoviesQExpr actorID = queryExpr
    (selectClause $ selectExpr Nothing)
      (selectColumns [O.fieldColumnName Nothing movieTitleField]) (Just
        (tableExpr (tableFromItem (O.tableName movieTable)
          `appendJoinFromItem`
            [
              joinExpr
                innerJoinType
                  (tableFromItem $ O.tableName movieActorTable)
                    (joinOnConstraint $
                      columnReference
                        (aliasQualifyColumn
                          (Just $ stringToAliasExpr "movies") (columnName "movie_id"))
                        `equals`
                      columnReference
                        (aliasQualifyColumn
                          (Just $ stringToAliasExpr "movie_actor") (columnName "movie_id")))
            ]
            )
          (Just $ whereClause
            (columnReference (O.fieldColumnName Nothing actorIDField)
              `equals`
            valueExpression (SqlValue.fromInt32 $ coerce actorID)))
            Nothing Nothing Nothing Nothing Nothing Nothing))

-- Agents queries
addAgentQ :: (MonadOrville m) => AgentWrite -> m ()
addAgentQ = O.insertEntity agentTable

fetchAllAgentsQ :: (MonadOrville m) => m [AgentRead]
fetchAllAgentsQ = O.findEntitiesBy agentTable O.emptySelectOptions

-- Actor Agent queries
addActorAgentQ :: (MonadOrville m) => ActorAgentWrite -> m ()
addActorAgentQ = O.insertEntity actorAgentTable

-- From the actor-agent table, get the actor name and agent name
viewActorsByAgentIDQ :: (MonadOrville m) => AgentID -> m [ActorRead]
viewActorsByAgentIDQ =
  execute
    ( findAll actorAgentTable agentIDField
        `chain` planList (focusParam getActorAgentReadID $ findOne actorTable actorIDField)
    )

fetchMovieBudgetAndTitleQ :: (MonadOrville m) => m [MovieInfo]
fetchMovieBudgetAndTitleQ = do
    Execution.executeAndDecode 
      Execution.SelectQuery 
        fetchMovieBudgetAndTitleQExpr
          (O.annotateSqlMarshallerEmptyAnnotation 
                movieInfoMarshaller)

fetchMovieBudgetAndTitleQExpr :: QueryExpr
fetchMovieBudgetAndTitleQExpr =
    queryExpr selectVals selectList (Just movieBudgetBoxOfficeExp)
  where
    selectVals = selectClause $ selectExpr Nothing
    selectList = selectColumns [
            O.fieldColumnName Nothing movieTitleField
          , O.fieldColumnName Nothing movieStatusField
          , O.fieldColumnName Nothing releaseDateField
          , O.fieldColumnName Nothing budgetField
          , O.fieldColumnName Nothing collectionField
        ]
    movieBudgetBoxOfficeExp = 
        tableExpr movieLeftJoinBudgetLeftJoinBoxOffice 
                Nothing Nothing Nothing Nothing Nothing Nothing Nothing
    movieLeftJoinBudgetLeftJoinBoxOffice =
        movieTableName 
            `appendJoinFromItem` [
                leftJoinMovieToMovieBudget
              , leftJoinMovieToMovieBoxOffice
            ]
    movieTableName = tableFromItem $ O.tableName movieTable
    movieBudgetTableName = tableFromItem $ O.tableName movieBudgetTable
    movieBoxOfficeTableName = tableFromItem $ O.tableName movieBoxOfficeTable
    leftJoinMovieToMovieBoxOffice =  
        joinExpr leftJoinType movieBoxOfficeTableName 
            $ joinOnConstraint (mkColumnReference "movies" "movie_id" `equals`
                 mkColumnReference "box_office_collection" "movie_id")
    leftJoinMovieToMovieBudget =
        joinExpr leftJoinType movieBudgetTableName 
            $ joinOnConstraint (mkColumnReference "movies" "movie_id" `equals`
                 mkColumnReference "movie_budget" "movie_id")
