
SELECT * FROM Actor;
SELECT * FROM Movies;
SELECT * FROM Actor WHERE actor_id = 1;
SELECT * FROM Movies WHERE title = 'Inception';

INSERT INTO Actor (first_name, last_name, gender, age) VALUES ('Leonardo', 'DiCaprio', 'Male', 46);

INSERT INTO Movies (title, status, release_date) VALUES ('Inception', 'Released', '2010-07-16');

INSERT INTO Director (first_name, last_name, gender, age) VALUES ('Christopher', 'Nolan', 'Male', 50);
