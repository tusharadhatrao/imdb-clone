begin;
-- DROP TYPE public."gender";

CREATE TYPE public."gender" AS ENUM (
	'Male',
	'Female',
	'Non-Binary',
	'Other');

-- DROP TYPE public."moviestatus";

CREATE TYPE public."moviestatus" AS ENUM (
	'Pre-Production',
	'Filming',
	'Post-Production',
	'Released');
-- public.actor definition

-- Drop table

-- DROP TABLE public.actor;

CREATE TABLE public.actor (
	actor_id serial4 NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NULL,
	"gender" public."gender" NULL,
	age int4 NULL,
	CONSTRAINT actor_age_check CHECK ((age >= 0)),
	CONSTRAINT actor_pkey PRIMARY KEY (actor_id)
);
CREATE INDEX idx_actor_last_name ON public.actor USING btree (last_name);


-- public.movies definition

-- Drop table

-- DROP TABLE public.movies;

CREATE TABLE public.movies (
	movie_id serial4 NOT NULL,
	title varchar(255) NOT NULL,
	status varchar(255) NULL,
	release_date date NULL,
	created_at timestamptz DEFAULT now() NULL,
	updated_at timestamptz DEFAULT now() NULL,
	CONSTRAINT movies_pkey PRIMARY KEY (movie_id)
);
CREATE INDEX idx_movie_title ON public.movies USING btree (title);


-- public.movie_actor definition

-- Drop table

-- DROP TABLE public.movie_actor;

CREATE TABLE public.movie_actor (
	movie_id int4 NOT NULL,
	actor_id int4 NOT NULL,
	"role" varchar(255) NULL,
	CONSTRAINT movie_actor_pkey PRIMARY KEY (movie_id, actor_id),
	CONSTRAINT movie_actor_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES public.actor(actor_id) ON DELETE CASCADE,
	CONSTRAINT movie_actor_movie_id_fkey FOREIGN KEY (movie_id) REFERENCES public.movies(movie_id) ON DELETE CASCADE
);

create table movie_budget (movie_id int primary key references movies on delete cascade,budget numeric not null);
insert into movie_budget values (6,5000000);

create or replace function box_office_movie_must_be_released()
returns trigger as $$
begin 
	if (select movie_status from movies where movie_id = new.movie_id) != 'Released' then 
		raise exception 'Movie is not released yet';
	end if;
	return new;
end;
$$ language plpgsql;

create or replace trigger before_insert_box_office_collection
before insert on box_office_collection
for each row
execute function box_office_movie_must_be_released();

alter table movie_budget add column created_at timestamptz default now();
alter table movie_budget add column updated_at timestamptz default now();
alter table box_office_collection add column updated_at timestamptz default now();
alter table box_office_collection add column created_at timestamptz default now();


create table agent (agent_id serial primary key
    ,agent_name varchar(255) not null
    ,salary_per_year numeric not null
    ,created_at timestamptz default now()
    ,updated_at timestamptz default now());


create table actor_agent (actor_id int primary key references actor on delete cascade,agent_id int references agent on delete cascade not null);

commit;
