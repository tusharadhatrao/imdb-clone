let Actor = {
    actorID : Integer
 ,  firstName : Text
 ,  lastName : Optional Text
 ,  gender : Text
 ,  age : Optional Integer
}

let actors : List Actor = [ 
    {actorID = +1, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +2, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +3, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +4, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +5, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +6, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +7, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +8, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  , {actorID = +9, firstName = "asd", lastName = Some "asd", gender = "asd", age = Some +1}
  ] 

in actors
