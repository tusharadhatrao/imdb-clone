module Main where

import IMDB.Platform.Core (startApp)
import Options.Applicative
import IMDB.Platform.Types

parseCmdArgs :: Parser CMDArgs
parseCmdArgs = CMDArgs
      <$> strOption
          ( long "connection_string"
         <> short 'd'
         <> metavar "CONN String"
         <> help "Connection string for postgresql" )
      <*> strOption
          ( long "log_file_path"
        <> short 'l'
        <> help "log file path")
      <*> strOption (
            long "log_level"
        <> short 'L' 
        <> help "log level")

main :: IO ()
main = do
  cmdArgs <- execParser opts
  startApp cmdArgs 
  where
    opts = info (parseCmdArgs <**> helper)
      ( fullDesc
     <> progDesc "IMDB Clone project for learning libraries"
     <> header "IMDB Clone in Haskell" )

