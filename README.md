# imdb-clone

This project has been created to learn below libraries:

- Servant / Wai
- Aeson
- Dhall
- Fast Logger
- graphql-parser
- Haxl
- Hedgehod, tasty
- Lens
- Mtl,unliftIO
- optparse-applicative
- Template haskell

## Notes
- The project will initially use [morpheus-graphql](https://github.com/morpheusgraphql/morpheus-graphql) to get the high level understanding of haskell+graphql and later will migrate to graphql-parser.

## Doubts

- How to insert enum types using orville?
- between operator not there?

## Resources

- [logs](https://github.com/alexwl/haskell-code-explorer/blob/2f1c2a4c87ebd55b8a335bc4670eec875af8b4c4/app/Indexer.hs#L183)
- [Haxl](https://github.com/facebook/Haxl/blob/main/example/facebook/readme.md)

## Doubts

- I Wrote the type for the tables, gave them generic to and from JSON instance. Made read and write entity out
  of it. In the API spec, I mentioned reqBody as `ActorWrite` i.e write entity, though aeson is still asking
  for non-writeable field (e.g actorID). I guess writing a FromJSON instance would resolve the issue, but 
  isn't there any other way?

## Running application

$ stack exec -- imdb-clone-exe -d "dbname=sample_orville host=localhost user=tushar password=1234" -l "/home/user/haskell/imdb-clone/imdb-logs.txt" -L "Debug"

