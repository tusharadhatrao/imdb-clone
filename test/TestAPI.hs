{-# LANGUAGE ScopedTypeVariables #-}
module TestAPI where

import Servant
import Orville.PostgreSQL
import Control.Concurrent.QSem
import IMDB.Platform.DataSource
import IMDB.Platform.Core
import Haxl.Core
import System.Log.FastLogger

connectionOptionsForTest :: ConnectionOptions
connectionOptionsForTest =
  ConnectionOptions
    { connectionString =
        "dbname=imdb_test_db host=localhost user=tushar password=1234",
      connectionNoticeReporting = DisableNoticeReporting,
      connectionPoolStripes = OneStripePerCapability,
      connectionPoolMaxConnections = MaxConnectionsPerStripe 1,
      connectionPoolLingerTime = 10
    }

testApp :: IO (Application,ConnectionPool)
testApp = do
  pool <- createConnectionPool connectionOptionsForTest
  loggerSet_ <- newFileLoggerSet defaultBufSize "/home/user/haskell/imdb-clone/imdb-logs.txt"
  sem <- newQSem 10 -- 10 threads
  (env0 :: Env () [Int]) <- initEnv (stateSet (IMDBState pool sem) stateEmpty) () 
  pure $ (app pool loggerSet_ env0 "debug",pool)
