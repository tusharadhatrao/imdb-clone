{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
module Utils where

import Data.ByteString.Lazy (ByteString)
import Data.Aeson
import IMDB.Platform.Model
import qualified IMDB.Platform.Model as A (Actor(..))

addMovieBody :: ByteString
addMovieBody = let sampleMovie = Movie {
    title = "Sample Movie"
  , status  = "Filming"
  , releaseDate = Nothing
 } in encode (sampleMovie :: Movie () ())

addActorBody :: ByteString
addActorBody = let sampleActor = Actor {
    A.actorID = ActorID 2
  , firstName = "Sample Actor"
  , lastName  = Just "Sample last name"
  , gender    = "Male"
  , age      = Just 21
  } in encode (sampleActor :: Actor ActorID)
