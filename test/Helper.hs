{-# LANGUAGE OverloadedStrings #-}
module Helper where

import Orville.PostgreSQL
import qualified Orville.PostgreSQL as O
import Orville.PostgreSQL.Raw.Connection
import Orville.PostgreSQL.AutoMigration
import IMDB.Platform.Table
import Orville.PostgreSQL.Expr
import Data.List.NonEmpty
import IMDB.Platform.Model
import Dhall

schemaList :: [SchemaItem]
schemaList = [
    SchemaTable movieTable 
  , SchemaTable actorTable
  , SchemaTable movieActorTable
  , SchemaTable agentTable
  , SchemaTable actorAgentTable
  , SchemaTable movieBudgetTable
  , SchemaTable movieBoxOfficeTable
 ]

schemaDropList :: [SchemaItem]
schemaDropList = [
    SchemaDropTable $ tableIdentifier movieTable 
  , SchemaDropTable $ tableIdentifier actorTable
  , SchemaDropTable $ tableIdentifier movieActorTable
  , SchemaDropTable $ tableIdentifier agentTable
  , SchemaDropTable $ tableIdentifier actorAgentTable
  , SchemaDropTable $ tableIdentifier movieBudgetTable
  , SchemaDropTable $ tableIdentifier movieBoxOfficeTable
 ]

setDefaultNow colName = alterColumnSetDefault (columnName colName) now

mkTableFieldDefault tableDef colName = alterTableExpr (O.tableName tableDef) 
        (setDefaultNow colName :| [])

createDB :: ConnectionPool -> IO ()
createDB pool = do
    actorList <- input Dhall.auto "./test.dhall"
    runOrville pool $ do
        autoMigrateSchema defaultOptions schemaList
        executeVoid DDLQuery $ mkTableFieldDefault movieTable "created_at"
        executeVoid DDLQuery $ mkTableFieldDefault movieTable "updated_at"
        mapM_ (\actor -> insertEntity actorTable actor) (actorList :: [ActorWrite])



truncateDB :: ConnectionPool -> IO ()
truncateDB pool = do
    print "inside truncate DB"
    runOrville pool $ autoMigrateSchema defaultOptions schemaDropList
