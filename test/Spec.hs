 {-# LANGUAGE OverloadedStrings #-}
import Test.Tasty
import Test.Tasty.HUnit
import Hedgehog
import Test.Tasty.Hedgehog
import Test.Tasty.Wai
import IMDB.Platform.Core
import TestAPI
import Servant
import Network.HTTP.Types
import Control.Monad.IO.Class
import Utils
import Helper
import qualified Orville.PostgreSQL as O
import qualified Orville.PostgreSQL.Raw.Connection as Conn
import IMDB.Platform.Query
import Control.Monad.Reader

tests :: Application -> O.ConnectionPool -> TestTree
tests app pool = testGroup "Tests" [
     unitTests pool
   , propertyTests
   , apiTests app
   ]

unitTests :: O.ConnectionPool -> TestTree
unitTests pool = testGroup "unitTests" [
        testCase "2+2" $
            [1, 2, 3] `compare` [1,2] @?= GT
      , testCase "Fetch movie by title db query" $ do
            res <- O.runOrville pool $ fetchMovieByTitleQ "Sample Movie"
            assertBool "" $ res /= Nothing 
    ]

propertyTests :: TestTree
propertyTests = testProperty "2 equlas 2" $ property $ 2 === 2

apiTests :: Application -> TestTree
apiTests app = testGroup "Servant IMDB" [ 
      testWai app "GET /fetch_movies - 200" $ do
        res <- get "/fetch_movies"
        assertStatus' status200 res
   ,  testWai app "Post /add_movie - 200" $ do
        res <- postWithHeaders "/add_movie" addMovieBody 
                    [("Content-Type", "application/json")]
        assertStatus' status200 res
   , testWai app "Get /view_actors - 200" $ do
        res <- get "/view_actors"
        assertStatus' status200 res
   , testWai app "Post /add_actor - 200" $ do
        res <- postWithHeaders "/add_actor" addActorBody 
                    [("Content-Type", "application/json")]
        assertStatus' status200 res
  --  , testWai app "Get fetch all movies with budget and collection" $ do
    --    res <- get "/fetch_movie_info"
      --  assertStatus' status200 res
   -- , testWai app "fetch titles by actor id" $ do
     --   res <- get "/fetch_titles_by_actorID/1"
       -- assertStatus' status200 res
  ]

main :: IO ()
main = do
  (app,pool) <- testApp
  createDB pool
  defaultMain $ tests app pool
  truncateDB pool
